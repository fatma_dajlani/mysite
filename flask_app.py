from flask import Flask, render_template, request, url_for, redirect
from pony.orm import Database, Required, Optional, PrimaryKey, select, db_session
#from flaskr.db import get_db


#from person import Person


app = Flask(__name__)
db = Database()

class Todo (db.Entity):
    id = PrimaryKey(int, auto= True)
    task_name = Required(str)
    duration = Required (int)
    location = Optional (str)

class ProductDb (db.Entity):
    id=PrimaryKey(int, auto= True)
    tname=Optional(str)
    name = Required(str)
    quantity = Required (int)
    price = Required (int)

db.bind (provider= 'sqlite', filename = 'productdb', create_db='True')
db.generate_mapping(create_tables= 'True')

@app.route('/todo2', methods=['GET', 'POST'])
@db_session
def todo2():

    new_things= []

    if request.method == 'GET':

        new_things= list(select(t for t in Todo))

        return render_template('todo.html', TODO=new_things)

    elif request.method == 'POST':

        task_name = request.form.get('task_name')
        duration = request.form.get('duration')
        location = request.form.get('location')

        Todo(task_name=task_name, duration= duration, location=location)

        new_things= list(select(t for t in Todo))

        return render_template('todo.html', TODO=new_things)




@app.route('/product', methods=['GET', 'POST'])
@db_session
def product():

    if request.method == 'GET':
        search = request.args.get('tname', '')
        product= select(p for p in ProductDb if search in p.name)
        return render_template('product_table.html', PRODUCT=product)

    elif request.method == 'POST':
        name = request.form.get('name')
        quantity = request.form.get('quantity')
        price = request.form.get('price')

        ProductDb(name=name, quantity = quantity, price = price)

        return redirect(url_for('product'))

@app.route('/delete/<id>')
@db_session
def delete(id):
    if ProductDb[id]:
        ProductDb[id].delete()
        return redirect(url_for('product'))

@app.route('/product/<id>/', methods=['GET', 'POST'])
@db_session
def modify(id):
    if request.method == 'GET':
        if ProductDb[id]:
            return render_template('modify.html', PRODUCT=ProductDb[id])

    elif request.method == 'POST':

        if ProductDb[id]:

            n = request.form.get('name')
            q = request.form.get('quantity')
            p = request.form.get('price')

            ProductDb[id].set(name=n, quantity=q, price= p)
            return redirect(url_for('product'))

if __name__ == '__main__':
    app.run(threaded=True, port=5000)









'''
@app.route('/', methods=['GET', 'POST'])
def index():

    if request.method == 'GET':

        return render_template('forms.html')

    elif request.method == 'POST':

        name = request.form.get('last_name')
        return f'hello {name}'



# table exercie
@app.route('/table')
def table():

    people = [Person('Butters, SouthPark, Student'),
                Person ('J Garrison, SouthPark, Teacher'),
                Person ('Homer, Springfield, Nuclear Safety Inspector.'),
                Person ('Randy, SouthPark, Farmer'),
                Person ('Rick, CuanaBacoa, Scientist')]
    return render_template ('table.html', PEOPLE = people)

#post get exercise



things = [{'task_name':'eat', 'duration': 10, 'location': 'home'},
            {'task_name':'sleep', 'duration': 9000, 'location': 'home'}]

@app.route('/todo', methods=['GET', 'POST'])
def todo():

    if request.method == 'GET':

        return render_template('todo.html', TODO=things)

    elif request.method == 'POST':


        lista = {'task_name': request.form.get('task_name'),
                    'duration': request.form.get('duration'),
                    'location': request.form.get('location')}

        things.append(lista)

        return render_template('todo.html', TODO=things)

        '''

